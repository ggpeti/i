# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}

variable "domain" {
  description = "The name of an existing Domain in your DigitalOcean account. Set it up here: https://cloud.digitalocean.com/networking/domains/"
  default = "eket.su"
}

variable "env" {
  description = "Name for the deployment environment, usually your name. This will be the subdomain for the server and appear in DigitalOcean resource names."
  default = "eket"
}

variable "do_region" {
  default = "fra1"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "tls_private_key" "eket" {
  algorithm = "RSA"
}

resource "local_file" "private_key" {
  filename = "${path.module}/SECRET_private_key"
  content  = "${tls_private_key.eket.private_key_pem}"

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/SECRET_private_key"
  }
}

resource "digitalocean_ssh_key" "eket" {
  name       = "eket-#{var.env}"
  public_key = "${tls_private_key.eket.public_key_openssh}"
}

data "template_file" "host-nixos-config" {
  template = <<EOF
# NixOS module for eket host system
# Provisioned by Terraform

let
  modulePath = "/root/eket-module.nix";
in
  { config, pkgs, lib, ... } :
    if lib.pathExists modulePath then
      {
        imports = [ modulePath ];

        services.eket.env = "${var.env}";
        services.eket.domain = "${var.domain}";

        system.autoUpgrade.enable = true;
      }
    else
      lib.warn ("NixOS module not found at " + modulePath + " . Skipping configuration.") {}
EOF
}

resource "digitalocean_droplet" "eket" {
  name       = "eket-${var.env}"
  size       = "s-1vcpu-1gb"
  image      = "ubuntu-16-04-x64"
  region     = "${var.do_region}"
  ssh_keys   = ["${digitalocean_ssh_key.eket.id}"]
  ipv6       = true

  private_networking = true

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.eket.ipv4_address}"
    private_key = "${tls_private_key.eket.private_key_pem}"
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/de3ef4794110f811f278643aa46ab3cce41a30c1/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "sed -i 's/reboot/shutdown -r -t 1/' /root/nixos-infect",
      "NIXOS_IMPORT=/root/host.nix NIX_CHANNEL=nixos-18.09 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
  }

  provisioner "local-exec" {
    command = "sleep 62"
  }
}

resource "digitalocean_floating_ip" "eket" {
  droplet_id = "${digitalocean_droplet.eket.id}"
  region     = "${digitalocean_droplet.eket.region}"
}

resource "digitalocean_record" "eket" {
  domain = "${var.domain}"
  type   = "A"
  name   = "@"
  value  = "${digitalocean_floating_ip.eket.ip_address}"
}

resource "digitalocean_record" "eket_subdomain" {
  domain = "${var.domain}"
  type   = "A"
  name   = "${var.env}"
  value  = "${digitalocean_floating_ip.eket.ip_address}"
}

resource "digitalocean_record" "eket_consul" {
  domain = "${var.domain}"
  type   = "A"
  name   = "consul"
  value  = "${digitalocean_floating_ip.eket.ip_address}"
}

resource "digitalocean_record" "eket_gore" {
  domain = "${var.domain}"
  type   = "A"
  name   = "gore"
  value  = "${digitalocean_floating_ip.eket.ip_address}"
}


resource "null_resource" "deploy" {
  depends_on = [ "digitalocean_record.eket" ]

  triggers {
    droplet = "${digitalocean_droplet.eket.id}"
    always  = "${uuid()}"
  }

  connection {
    user        = "root"
    host        = "${digitalocean_floating_ip.eket.ip_address}"
    private_key = "${tls_private_key.eket.private_key_pem}"
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello, World!'"]
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "file" {
    source     = "eket-module.nix"
    destination = "/root/eket-module.nix"
  }

  provisioner "remote-exec" {
    inline = ["nixos-rebuild build --show-trace && nixos-rebuild switch"]
  }
}

output "ip" {
  value = "${digitalocean_floating_ip.eket.ip_address}"
}

output "fqdn" {
  value = "${digitalocean_record.eket.fqdn}"
}

output "private_key_file" {
  value = "${path.module}/SECRET_private_key"
}
