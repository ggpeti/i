# Define a pinned nixpkgs version
{ pkgs ? import <nixpkgs> {}, ...} :
let
  pinned_pkgs_path = pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "80738ed9dc0ce48d7796baed5364eef8072c794d";
    sha256 = "0anmvr6b47gbbyl9v2fn86mfkcwgpbd5lf0yf3drgm8pbv57c1dc";
  };
in
  import pinned_pkgs_path {}
